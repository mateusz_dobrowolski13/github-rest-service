package com.dobrowolski.rest.handlers;

import com.dobrowolski.rest.handlers.response.ResponseHandlerFactory;
import com.dobrowolski.rest.service.GitRepositoryService;
import io.netty.channel.ConnectTimeoutException;
import io.netty.handler.timeout.ReadTimeoutException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@Component
@Slf4j
@RequiredArgsConstructor
public class RepositoryHandler {
    private static final String OWNER = "owner";
    private static final String REPOSITORY_NAME = "repositoryName";

    private final GitRepositoryService gitRepositoryService;
    private final ResponseHandlerFactory responseHandlerFactory;

    public Mono<ServerResponse> getRepository(ServerRequest serverRequest) {
        return gitRepositoryService.getRepositoryDetails(
                serverRequest.pathVariable(OWNER),
                serverRequest.pathVariable(REPOSITORY_NAME),
                serverRequest.headers().asHttpHeaders().getIfModifiedSince())
                .doOnError(ReadTimeoutException.class, e -> {throw new ResponseStatusException(HttpStatus.GATEWAY_TIMEOUT, "Github API is unreachable");})
                .doOnError(ConnectTimeoutException.class, e -> {throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Github API is unavailable");})
                .flatMap(clientResponse -> responseHandlerFactory
                        .getResponseHandler(clientResponse.statusCode())
                        .handleResponse(clientResponse)
                );
    }

}
