package com.dobrowolski.rest.handlers.response;

import org.springframework.http.HttpStatus;

public interface ResponseHandlerFactory {
    ResponseHandler getResponseHandler(HttpStatus httpStatus);
}
