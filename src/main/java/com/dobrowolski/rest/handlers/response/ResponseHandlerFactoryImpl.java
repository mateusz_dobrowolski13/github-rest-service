package com.dobrowolski.rest.handlers.response;

import com.dobrowolski.rest.dto.GitRepository;
import com.dobrowolski.rest.dto.RepositoryResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.time.Instant;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@Component
@RequiredArgsConstructor
public class ResponseHandlerFactoryImpl implements ResponseHandlerFactory {
    private ResponseHandler SUCCESSFUL_RESPONSE_HANDLER = new SuccessfulResponseHandler();
    private ResponseHandler NOT_MODIFIED_RESPONSE_HANDLER = new NotModifiedResponseHandler();
    private ResponseHandler NOT_FOUND_RESPONSE_HANDLER = new NotFoundResponseHandler();
    private ResponseHandler UNAUTHORIZED_RESPONSE_HANDLER = new UnauthorizedResponseHandler();
    private ResponseHandler FORBIDDEN_RESPONSE_HANDLER = new ForbiddenResponseHandler();
    private ResponseHandler UNAVAILABLE_RESPONSE_HANDLER = new UnavailableResponseHandler();
    private ResponseHandler DEFAULT_RESPONSE_HANDLER = new DefaultResponseHandler();
    private final ModelMapper modelMapper;

    public ResponseHandler getResponseHandler(HttpStatus httpStatus) {
        switch (httpStatus) {
            case OK:
                return SUCCESSFUL_RESPONSE_HANDLER;
            case NOT_MODIFIED:
                return NOT_MODIFIED_RESPONSE_HANDLER;
            case NOT_FOUND:
                return NOT_FOUND_RESPONSE_HANDLER;
            case UNAUTHORIZED:
                return UNAUTHORIZED_RESPONSE_HANDLER;
            case FORBIDDEN:
                return FORBIDDEN_RESPONSE_HANDLER;
            default:
                if (httpStatus.is5xxServerError()) {
                    return UNAVAILABLE_RESPONSE_HANDLER;
                }
                return DEFAULT_RESPONSE_HANDLER;
        }
    }

    private class SuccessfulResponseHandler implements ResponseHandler {
        @Override
        public Mono<ServerResponse> handleResponse(ClientResponse clientResponse) {
            return clientResponse.toEntity(GitRepository.class)
                    .flatMap(responseEntity -> ServerResponse.ok()
                            .lastModified(Instant.ofEpochMilli(responseEntity.getHeaders().getLastModified()))
                            .body(BodyInserters.fromObject(modelMapper.map(responseEntity.getBody(), RepositoryResponse.class)))
                    );
        }
    }

    private static class NotModifiedResponseHandler implements ResponseHandler {
        @Override
        public Mono<ServerResponse> handleResponse(ClientResponse clientResponse) {
            clientResponse.bodyToMono(Void.class);
            return ServerResponse.status(NOT_MODIFIED)
                    .lastModified(Instant.ofEpochMilli(clientResponse.headers().asHttpHeaders().getLastModified()))
                    .build();
        }
    }

    private static class NotFoundResponseHandler implements ResponseHandler {
        @Override
        public Mono<ServerResponse> handleResponse(ClientResponse clientResponse) {
            return Mono.error(new ResponseStatusException(NOT_FOUND, "Not a valid owner or repository tag."));
        }
    }

    private static class UnauthorizedResponseHandler implements ResponseHandler {
        @Override
        public Mono<ServerResponse> handleResponse(ClientResponse clientResponse) {
            return Mono.error(new ResponseStatusException(UNAUTHORIZED, "Check login data to the GitHub API in the application properties."));
        }
    }

    private static class ForbiddenResponseHandler implements ResponseHandler {
        @Override
        public Mono<ServerResponse> handleResponse(ClientResponse clientResponse) {
            return Mono.error(new ResponseStatusException(FORBIDDEN, "We've exceeded the Github limit."));
        }
    }

    private static class UnavailableResponseHandler implements ResponseHandler {
        @Override
        public Mono<ServerResponse> handleResponse(ClientResponse clientResponse) {
            return Mono.error(new ResponseStatusException(SERVICE_UNAVAILABLE, "The GitHub API is unavailable."));
        }
    }

    @Slf4j
    private static class DefaultResponseHandler implements ResponseHandler {
        @Override
        public Mono<ServerResponse> handleResponse(ClientResponse clientResponse) {
            return clientResponse.toEntity(String.class)
                    .doOnNext(responseEntity -> log.info("Status={}, Body={}", responseEntity.getStatusCode(), responseEntity.getBody()))
                    .flatMap(responseEntity -> ServerResponse.status(responseEntity.getStatusCode())
                            .contentType(APPLICATION_JSON_UTF8)
                            .body(BodyInserters.fromObject(responseEntity.getBody()))
                    );
        }
    }

}