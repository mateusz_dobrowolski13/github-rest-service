package com.dobrowolski.rest.handlers.response;

import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

public interface ResponseHandler {
    Mono<ServerResponse> handleResponse(ClientResponse clientResponse);
}