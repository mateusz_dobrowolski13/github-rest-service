package com.dobrowolski.rest.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class GitRepositoryServiceImpl implements GitRepositoryService {

    private final WebClient webClient;

    public Mono<ClientResponse> getRepositoryDetails(String owner, String repositoryName, final long requestModifiedDate) {
        return webClient.get()
                .uri("/repos/{owner}/{repository-name}", owner, repositoryName)
                .headers(httpEntity -> {
                    if (requestModifiedDate != -1) {
                        httpEntity.setIfModifiedSince(requestModifiedDate);
                    }
                })
                .exchange();
    }
}
