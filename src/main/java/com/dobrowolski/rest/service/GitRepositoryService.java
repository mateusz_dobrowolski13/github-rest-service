package com.dobrowolski.rest.service;

import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

public interface GitRepositoryService {
    Mono<ClientResponse> getRepositoryDetails(String owner, String repositoryName, long ifModifiedSince);
}
