package com.dobrowolski.rest.config;

import com.dobrowolski.rest.handlers.RepositoryHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

@Configuration
@RequiredArgsConstructor
class RouterConfig {

    private final RepositoryHandler repositoryApi;

    @Bean
    public RouterFunction route() {
        return RouterFunctions
                .route(GET("/repositories/{owner}/{repositoryName}"), repositoryApi::getRepository);
    }

}
