package com.dobrowolski.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class GitRepository {
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("clone_url")
    private String cloneUrl;
    @JsonProperty("stargazers_count")
    private int stars;
    @JsonProperty("created_at")
    private Date createdAt;
    @JsonProperty("name")
    private String name;

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public String getCloneUrl() {
        return cloneUrl;
    }

    public int getStars() {
        return stars;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
