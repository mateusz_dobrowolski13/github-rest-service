package com.dobrowolski.rest.dto;

import java.util.Date;

public class RepositoryResponse {
    private String fullName;
    private String description;
    private String cloneUrl;
    private int stars;
    private Date createdAt;

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public String getCloneUrl() {
        return cloneUrl;
    }

    public int getStars() {
        return stars;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
