package com.dobrowolski.rest.util;

public class FieldEnum {

    public enum ResponseFields {
        FULL_NAME("fullName"),
        STARS("stars"),
        DESCRIPTION("description"),
        CLONE_URL("cloneUrl"),
        CREATED_AT("createdAt"),
        NAME("name");

        private String name;

        ResponseFields(String name) {
            this.name = name;
        }

        public String toJsonField() {
            return String.format("$.%s", name);
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum ErrorResponseFields {
        TIMESTAMP("timestamp"),
        PATH("path"),
        STATUS("status"),
        ERROR("error"),
        MESSAGE("message");

        private String name;

        ErrorResponseFields(String name) {
            this.name = name;
        }

        public String toJsonField() {
            return String.format("$.%s", name);
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
