package com.dobrowolski.rest;

import com.dobrowolski.rest.annotation.IntegrationTest;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.dobrowolski.rest.util.FieldEnum.ErrorResponseFields.*;
import static com.dobrowolski.rest.util.FieldEnum.ResponseFields.*;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.HttpStatus.*;

@ActiveProfiles("stub")
@Category(IntegrationTest.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@org.springframework.cloud.contract.wiremock.AutoConfigureWireMock(port = 5000)
public class RepositoryApiIT {
    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void testSuccessfulResponse() {
        assert200Response("octokit", "octokit.rb", "https://github.com/octokit/octokit.rb.git", "Ruby toolkit for the GitHub API", 3108);
    }

    @Test
    public void testBraders2Repository() {
        assert200Response("braders2", "ProjectTest", "https://github.com/braders2/ProjectTest.git", "Test", 0);
        assert304Response("braders2", "ProjectTest");
        assert403Response("braders2", "ProjectTest");
    }

    @Test
    public void testNotFoundResponse() {
        assertErrorResponse("braders2", "ProjectTes", "/repositories/braders2/ProjectTes", NOT_FOUND, "Not a valid owner or repository tag.");
    }

    @Test
    public void testUnauthorized() {
        assertErrorResponse("braders2", "Allegro", "/repositories/braders2/Allegro", UNAUTHORIZED, "Check login data to the GitHub API in the application properties.");
    }

    @Test
    public void testGitApiInternalError() {
        WireMock.stubFor(WireMock.get("/repos/braders2/Internal").willReturn(WireMock.serviceUnavailable()));
        assertErrorResponse("braders2", "Internal", "/repositories/braders2/Internal", SERVICE_UNAVAILABLE, "The GitHub API is unavailable.");
    }

    @Test
    public void testGitApiDefaultHandler() {
        WireMock.stubFor(WireMock.get("/repos/braders2/Default").willReturn(WireMock.aResponse()
                .withStatus(MOVED_PERMANENTLY.value()).withBody("{\"test\":\"Test\"}")));

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", "braders2", "Default")
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(MOVED_PERMANENTLY)
                .expectBody()
                .jsonPath("$.*").value(hasSize(1))
                .jsonPath("$.test").isEqualTo("Test");
    }

    private void assert200Response(String owner, String repositoryName, String expectedCloneUrl, String expectedDescription, int expectedStarsCount) {
        String expectedFullName = owner + "/" + repositoryName;

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", owner, repositoryName)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .exchange()
                .expectHeader().exists(HttpHeaders.LAST_MODIFIED)
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isOk()
                .expectBody()
                .jsonPath(CREATED_AT.toJsonField()).exists()
                .jsonPath(FULL_NAME.toJsonField()).isEqualTo(expectedFullName)
                .jsonPath(DESCRIPTION.toJsonField()).isEqualTo(expectedDescription)
                .jsonPath(CLONE_URL.toJsonField()).isEqualTo(expectedCloneUrl)
                .jsonPath(STARS.toJsonField()).isEqualTo(expectedStarsCount)
                .jsonPath("$.*").value(hasSize(5));
    }

    private void assert304Response(String owner, String repositoryName) {
        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", owner, repositoryName)
                .headers(httpHeaders -> httpHeaders.setIfModifiedSince(1563433009000L))
                .exchange()
                .expectStatus().isNotModified()
                .expectHeader().exists(HttpHeaders.LAST_MODIFIED)
                .expectBody().isEmpty();
    }

    private void assert403Response(String owner, String repositoryName) {
        var bodyContentSpec = webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", owner, repositoryName)
                .headers(httpHeaders -> httpHeaders.setIfModifiedSince(1563433009000L))
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isForbidden()
                .expectBody();
        assertJsonPath(bodyContentSpec, String.format("/repositories/%s/%s", owner, repositoryName), FORBIDDEN, "We've exceeded the Github limit.");
    }

    private void assertErrorResponse(String owner, String repositoryName, String expectedPath, HttpStatus expectedStatus, String expectedMessage) {
        var bodyContentSpec = webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", owner, repositoryName)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(expectedStatus)
                .expectBody();
        assertJsonPath(bodyContentSpec, expectedPath, expectedStatus, expectedMessage);
    }

    private void assertJsonPath(WebTestClient.BodyContentSpec bodyContentSpec, String expectedPath, HttpStatus expectedStatus, String expectedMessage) {
        bodyContentSpec.jsonPath(TIMESTAMP.toJsonField()).exists()
                .jsonPath(PATH.toJsonField()).isEqualTo(expectedPath)
                .jsonPath(STATUS.toJsonField()).isEqualTo(expectedStatus.value())
                .jsonPath(ERROR.toJsonField()).isEqualTo(expectedStatus.getReasonPhrase())
                .jsonPath(MESSAGE.toJsonField()).isEqualTo(expectedMessage);
    }

}
