package com.dobrowolski.rest;

import com.dobrowolski.rest.annotation.UnitTest;
import com.dobrowolski.rest.dto.GitRepository;
import com.dobrowolski.rest.handlers.response.ResponseHandlerFactory;
import com.dobrowolski.rest.handlers.response.ResponseHandlerFactoryImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpStatus.*;

@Category(UnitTest.class)
@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class ResponseHandlerFactoryTest {

    @Mock
    ClientResponse clientResponse;

    @InjectMocks
    private ModelMapper modelMapper = new ModelMapper();

    @InjectMocks
    private ResponseHandlerFactory responseHandlerFactory = new ResponseHandlerFactoryImpl(modelMapper);


    @Before
    public void setUp() {
        mockClientResponse();
    }

    @Test
    public void testFactory() {
        assertThat(responseHandlerFactory.getResponseHandler(OK).getClass().getName(), containsString("SuccessfulResponseHandler"));
        assertThat(responseHandlerFactory.getResponseHandler(NOT_MODIFIED).getClass().getName(), containsString("NotModifiedResponseHandler"));
        assertThat(responseHandlerFactory.getResponseHandler(NOT_FOUND).getClass().getName(), containsString("NotFoundResponseHandler"));
        assertThat(responseHandlerFactory.getResponseHandler(UNAUTHORIZED).getClass().getName(), containsString("UnauthorizedResponseHandler"));
        assertThat(responseHandlerFactory.getResponseHandler(FORBIDDEN).getClass().getName(), containsString("ForbiddenResponseHandler"));
        assertThat(responseHandlerFactory.getResponseHandler(SERVICE_UNAVAILABLE).getClass().getName(), containsString("UnavailableResponseHandler"));
        assertThat(responseHandlerFactory.getResponseHandler(MOVED_PERMANENTLY).getClass().getName(), containsString("DefaultResponseHandler"));
    }

    @Test
    public void testSuccessfulResponseHandler() {
        Mockito.when(clientResponse.toEntity(GitRepository.class))
                .thenReturn(Mono.just(ResponseEntity.status(OK).lastModified(1563142131000L)
                        .body(new GitRepository())));
        var serverResponse = responseHandlerFactory.getResponseHandler(OK).handleResponse(clientResponse);
        StepVerifier.create(serverResponse)
                .consumeNextWith(response -> {
                            assertEquals(OK, response.statusCode());
                            assertEquals(1563142131000L, response.headers().getLastModified());
                        }
                ).expectComplete()
                .verify();
    }

    @Test
    public void testNotModifiedResponseHandler() {
        var serverResponse = responseHandlerFactory.getResponseHandler(NOT_MODIFIED).handleResponse(clientResponse);
        StepVerifier.create(serverResponse)
                .consumeNextWith(response -> {
                    assertEquals(NOT_MODIFIED, response.statusCode());
                    assertEquals(1563142131000L, response.headers().getLastModified());
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void testUnauthorizedResponseHandler() {
        var serverResponse = responseHandlerFactory.getResponseHandler(UNAUTHORIZED).handleResponse(clientResponse);
        StepVerifier.create(serverResponse)
                .expectErrorMessage("401 UNAUTHORIZED \"Check login data to the GitHub API in the application properties.\"")
                .verify();
    }

    @Test
    public void testForbiddenResponseHandler() {
        var serverResponse = responseHandlerFactory.getResponseHandler(FORBIDDEN).handleResponse(clientResponse);
        StepVerifier.create(serverResponse)
                .expectErrorMessage("403 FORBIDDEN \"We've exceeded the Github limit.\"")
                .verify();
    }

    @Test
    public void testNotFoundResponseHandler() {
        var serverResponse = responseHandlerFactory.getResponseHandler(NOT_FOUND).handleResponse(clientResponse);
        StepVerifier.create(serverResponse)
                .expectErrorMessage("404 NOT_FOUND \"Not a valid owner or repository tag.\"")
                .verify();
    }

    @Test
    public void testDefaultResponseHandler() {
        Mockito.when(clientResponse.toEntity(String.class))
                .thenReturn(Mono.just(ResponseEntity.status(MOVED_PERMANENTLY).body("test")));
        var serverResponse = responseHandlerFactory.getResponseHandler(MOVED_PERMANENTLY).handleResponse(clientResponse);
        StepVerifier.create(serverResponse)
                .consumeNextWith(response -> assertEquals(MOVED_PERMANENTLY, response.statusCode()))
                .verifyComplete();
    }

    private void mockClientResponse() {
        var clientResponseHeaders = Mockito.mock(ClientResponse.Headers.class);
        var httpHeaders = Mockito.mock(org.springframework.http.HttpHeaders.class);
        Mockito.when(clientResponse.headers()).thenReturn(clientResponseHeaders);
        Mockito.when(clientResponseHeaders.asHttpHeaders()).thenReturn(httpHeaders);
        Mockito.when(httpHeaders.getLastModified()).thenReturn(1563142131000L);
    }

}
