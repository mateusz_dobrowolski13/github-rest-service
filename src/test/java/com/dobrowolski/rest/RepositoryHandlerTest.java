package com.dobrowolski.rest;

import com.dobrowolski.rest.annotation.UnitTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static com.dobrowolski.rest.util.FieldEnum.ErrorResponseFields.*;
import static com.dobrowolski.rest.util.FieldEnum.ResponseFields.*;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.notNull;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.*;

@Category(UnitTest.class)
@RunWith(SpringRunner.class)
@Import({RestServiceApplication.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RepositoryHandlerTest {
    @MockBean
    WebClient webClient;

    @Autowired
    WebTestClient webTestClient;

    @Test
    public void testSuccessfulResponse() {
        var clientResponse = ClientResponse.create(OK)
                .body("{\"full_name\":\"octokit/octokit.rb\",\"clone_url\":\"https://github.com/octokit/octokit.rb.git\",\"stargazers_count\":3103,\"created_at\":\"2009-12-10T21:41:49.000+0000\",\"description\":\"Ruby toolkit for the GitHub API\"}")
                .headers(httpHeaders -> {
                    httpHeaders.setLastModified(1563142131000L);
                    httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
                })
                .build();
        mockWebClientResponse(clientResponse);

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", "octokit", "octokit.rb")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectHeader().lastModified(1563142131000L)
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isOk()
                .expectBody()
                .jsonPath(FULL_NAME.toJsonField()).isEqualTo("octokit/octokit.rb")
                .jsonPath(DESCRIPTION.toJsonField()).isEqualTo("Ruby toolkit for the GitHub API")
                .jsonPath(CLONE_URL.toJsonField()).isEqualTo("https://github.com/octokit/octokit.rb.git")
                .jsonPath(STARS.toJsonField()).isEqualTo("3103")
                .jsonPath(CREATED_AT.toJsonField()).exists()
                .jsonPath(NAME.toJsonField()).doesNotExist();
    }

    @Test
    public void testNotModifiedResponse() {
        var clientResponse = ClientResponse.create(NOT_MODIFIED)
                .headers(httpHeaders -> {
                    httpHeaders.setLastModified(1563142131000L);
                    httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
                })
                .build();
        mockWebClientResponse(clientResponse);

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", "octokit", "octokit.rb")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectHeader().lastModified(1563142131000L)
                .expectStatus().isNotModified()
                .expectBody().isEmpty();
    }

    @Test
    public void testNotFoundResponse() {
        var clientResponse = ClientResponse.create(NOT_FOUND)
                .headers(httpHeaders -> httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8))
                .build();
        mockWebClientResponse(clientResponse);

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", "octokit", "octokit.rb")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(NOT_FOUND)
                .expectBody()
                .jsonPath(TIMESTAMP.toJsonField()).exists()
                .jsonPath(STATUS.toJsonField()).isEqualTo(NOT_FOUND.value())
                .jsonPath(PATH.toJsonField()).isEqualTo("/repositories/octokit/octokit.rb")
                .jsonPath(ERROR.toJsonField()).isEqualTo(NOT_FOUND.getReasonPhrase())
                .jsonPath(MESSAGE.toJsonField()).isEqualTo("Not a valid owner or repository tag.");
    }

    @Test
    public void testUnauthorizedResponse() {
        var clientResponse = ClientResponse.create(UNAUTHORIZED)
                .headers(httpHeaders -> httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8))
                .build();
        mockWebClientResponse(clientResponse);

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", "octokit", "octokit.rb")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody()
                .jsonPath(TIMESTAMP.toJsonField()).exists()
                .jsonPath(STATUS.toJsonField()).isEqualTo(UNAUTHORIZED.value())
                .jsonPath(PATH.toJsonField()).isEqualTo("/repositories/octokit/octokit.rb")
                .jsonPath(ERROR.toJsonField()).isEqualTo(UNAUTHORIZED.getReasonPhrase())
                .jsonPath(MESSAGE.toJsonField()).isEqualTo("Check login data to the GitHub API in the application properties.");
    }

    @Test
    public void testForbiddenResponse() {
        var clientResponse = ClientResponse.create(FORBIDDEN)
                .headers(httpHeaders -> httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8))
                .build();
        mockWebClientResponse(clientResponse);

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", "octokit", "octokit.rb")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(FORBIDDEN)
                .expectBody()
                .jsonPath(TIMESTAMP.toJsonField()).exists()
                .jsonPath(STATUS.toJsonField()).isEqualTo(FORBIDDEN.value())
                .jsonPath(PATH.toJsonField()).isEqualTo("/repositories/octokit/octokit.rb")
                .jsonPath(ERROR.toJsonField()).isEqualTo(FORBIDDEN.getReasonPhrase())
                .jsonPath(MESSAGE.toJsonField()).isEqualTo("We've exceeded the Github limit.");
    }

    @Test
    public void testServiceUnavailableResponse() {
        var clientResponse = ClientResponse.create(INTERNAL_SERVER_ERROR)
                .headers(httpHeaders -> httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8))
                .build();
        mockWebClientResponse(clientResponse);

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", "octokit", "octokit.rb")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(SERVICE_UNAVAILABLE)
                .expectBody()
                .jsonPath(TIMESTAMP.toJsonField()).exists()
                .jsonPath(STATUS.toJsonField()).isEqualTo(SERVICE_UNAVAILABLE.value())
                .jsonPath(PATH.toJsonField()).isEqualTo("/repositories/octokit/octokit.rb")
                .jsonPath(ERROR.toJsonField()).isEqualTo(SERVICE_UNAVAILABLE.getReasonPhrase())
                .jsonPath(MESSAGE.toJsonField()).isEqualTo("The GitHub API is unavailable.");
    }

    @Test
    public void testOtherResponse() {
        var clientResponse = ClientResponse.create(MOVED_PERMANENTLY)
                .headers(httpHeaders -> httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8))
                .body("{\"test\": \"Test\"}")
                .build();
        mockWebClientResponse(clientResponse);

        webTestClient.get()
                .uri("/repositories/{owner}/{repositoryName}", "octokit", "octokit.rb")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(MOVED_PERMANENTLY)
                .expectBody()
                .jsonPath("$.*").value(hasSize(1))
                .jsonPath("$.test").isEqualTo("Test");
    }


    private void mockWebClientResponse(final ClientResponse resp) {
        final var uriSpecMock = Mockito.mock(WebClient.RequestHeadersUriSpec.class);
        final var headersSpecMock = Mockito.mock(WebClient.RequestHeadersSpec.class);

        when(webClient.get()).thenReturn(uriSpecMock);
        when(uriSpecMock.uri(notNull(), any(), any())).thenReturn(headersSpecMock);
        when(headersSpecMock.header(notNull(), notNull())).thenReturn(headersSpecMock);
        when(headersSpecMock.headers(notNull())).thenReturn(headersSpecMock);
        when(headersSpecMock.exchange()).thenReturn(Mono.just(resp));
    }

}
